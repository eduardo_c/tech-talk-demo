package com.techtalk.eduardo.techtalkdemo.interfaces;

import java.util.List;

public interface ResultsView {
    void setResults(final List<Classifier.Recognition> results);
}
