package com.techtalk.eduardo.techtalkdemo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.media.Image;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.techtalk.eduardo.techtalkdemo.R;

public class DetectionImage extends AppCompatImageView {

    public enum CharacterDetected{KOOPA, MARIO, NOTHING}

    private CharacterDetected characterDetected;

    public DetectionImage(Context context) {
        this(context, null, 0);
        characterDetected = null;
    }

    public DetectionImage(Context context, AttributeSet set){
        this(context, set, 0);
    }

    public DetectionImage(Context context, AttributeSet set, int defStyle){
        super(context, set, 0);
        setWillNotDraw(false);
    }

    public void setIsBest(CharacterDetected best){
        characterDetected = best;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(characterDetected == CharacterDetected.KOOPA)
            setImageResource(R.drawable.koopa2);
        else if(characterDetected == CharacterDetected.MARIO)
            setImageResource(R.drawable.mario2);
        else {
            setImageResource(R.drawable.techdojo);
        }
    }
}
